<?php

namespace TextMedia\OfflineLockerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Бандл пессимистичных автономных блокировок (для бизнес-транзакций)
 */
class OfflineLockerBundle extends Bundle
{
}
