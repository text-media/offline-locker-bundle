<?php

namespace TextMedia\OfflineLockerBundle;

use Doctrine\ORM\EntityRepository;

interface OfflineLockerInterface
{
    /**
     * Получение блокировки
     *
     * @param string $key   Ключ блокировки
     * @param string $owner Владелец блокировки
     */
    public function acquireLock(string $key, string $owner = '');

    /**
     * Освобождение блокировки
     *
     * @param string $key   Ключ блокировки
     * @param string $owner Владелец блокировки
     */
    public function releaseLock(string $key, string $owner = '');

    /**
     * Проверка наличия блокировки
     *
     * @param string $key   Ключ блокировки
     * @param string $owner Владелец блокировки
     *
     * @return bool
     */
    public function hasLock(string $key, string $owner): bool;

    /**
     * Проверка наличия любой блокировки
     *
     * @param string $key   Ключ блокировки
     *
     * @return bool
     */
    public function hasAnyLock(string $key): bool;

    /**
     * Возвращает репозиторий хранения блокировок
     *
     * @return EntityRepository
     */
    public function getRepository(): EntityRepository;
}
