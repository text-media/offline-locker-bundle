<?php

namespace TextMedia\OfflineLockerBundle;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use TextMedia\OfflineLockerBundle\Entity\Lock;

class OfflineLocker implements OfflineLockerInterface
{
     /**
     * @var RegistryInterface
     */
    private $doctrine;

    /**
     * @var string
     */
    private $entityManagerName;

    /**
     * @var int
     */
    protected $isolationLevel;

    /**
     * LockManager constructor.
     *
     * @param RegistryInterface $doctrine
     * @param string   $entityManagerName
     */
    public function __construct(RegistryInterface $doctrine, string $entityManagerName)
    {
        $this->doctrine          = $doctrine;
        $this->entityManagerName = $entityManagerName;
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        /** @var EntityManagerInterface $em */
        $em = $this->doctrine->getManager($this->entityManagerName);

        if (!$em->isOpen()) {
            $em = $this->doctrine->resetManager($this->entityManagerName);
        }

        return $em;
    }

    public function getRepository(): EntityRepository
    {
        return $this->getEntityManager()->getRepository(Lock::class);
    }

    /**
     * @inheritdoc
     */
    public function acquireLock(string $key, string $owner = '')
    {
        $this->beginTransaction();

        $this->getEntityManager()->persist(new Lock($key, $owner));

        try {
            $this->commitTransaction();
        } catch (UniqueConstraintViolationException $exception) {
            $this->rollbackTransaction();
            throw new ConcurrencyException('Дождитесь завершения операции');
        }
    }

    /**
     * @inheritdoc
     */
    public function releaseLock(string $key, string $owner = '')
    {
        $this->beginTransaction();

        $lock = $this->getRepository()->find($key, LockMode::PESSIMISTIC_WRITE);

        if ($lock) {
            if ($lock->getOwner() !== $owner) {
                $this->rollbackTransaction();
                throw new ConcurrencyException('Блокировка найдена, но вы не являетесь её владельцем');
            }
            $this->getEntityManager()->remove($lock);
        }

        $this->commitTransaction();
    }

    /**
     * @inheritdoc
     */
    public function hasLock(string $key, string $owner): bool
    {
        $this->beginTransaction();
        $lock = $this->getRepository()->find($key, LockMode::PESSIMISTIC_READ);
        $this->commitTransaction();

        return (bool) $lock && $lock->getOwner() === $owner;
    }

    /**
     * @inheritdoc
     */
    public function hasAnyLock(string $key): bool
    {
        $this->beginTransaction();
        $lock = $this->getRepository()->find($key, LockMode::PESSIMISTIC_READ);
        $this->commitTransaction();

        return (bool) $lock;
    }

    /**
     * Открыть транзакцию (SERIALIZABLE)
     */
    protected function beginTransaction()
    {
        $em = $this->getEntityManager();
        $this->isolationLevel = $em->getConnection()->getTransactionIsolation();
        $em->getConnection()->setTransactionIsolation(Connection::TRANSACTION_SERIALIZABLE);
        $em->getConnection()->beginTransaction();
    }

    /**
     * Зафиксировать транзакцию (SERIALIZABLE)
     */
    protected function commitTransaction()
    {
        $em = $this->getEntityManager();
        $em->flush();
        $em->getConnection()->commit();
        $em->getConnection()->setTransactionIsolation($this->isolationLevel);
    }

    /**
     * Откатить транзакцию (SERIALIZABLE)
     */
    protected function rollbackTransaction()
    {
        $em = $this->getEntityManager();
        $em->getConnection()->rollBack();
        $em->getConnection()->setTransactionIsolation($this->isolationLevel);
    }

    /**
     * Выполнить flush (SERIALIZABLE)
     *
     * @throws \Exception
     */
    protected function flush()
    {
        $em = $this->getEntityManager();
        $this->isolationLevel = $em->getConnection()->getTransactionIsolation();
        $em->getConnection()->setTransactionIsolation(Connection::TRANSACTION_SERIALIZABLE);

        try {
            $em->flush();
        } catch (\Exception $exception) {
            $em->getConnection()->setTransactionIsolation($this->isolationLevel);
            throw $exception;
        }

        $em->getConnection()->setTransactionIsolation($this->isolationLevel);
    }
}
