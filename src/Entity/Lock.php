<?php

namespace TextMedia\OfflineLockerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Блокировка
 *
 * @ORM\Table(name="offline_locker_locks", indexes={
 *     @ORM\Index(name="IDX_lock_owner", columns={"owner"}),
 * })
 * @ORM\Entity
 */
class Lock
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="string")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="owner", type="string")
     */
    private $owner;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * Lock constructor.
     *
     * @param string $id
     * @param string $owner
     */
    public function __construct(string $id, string $owner)
    {
        $this->id        = $id;
        $this->owner     = $owner;
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getOwner(): string
    {
        return $this->owner;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }
}
