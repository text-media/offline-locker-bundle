<?php

namespace TextMedia\OfflineLockerBundle\Command;

use TextMedia\OfflineLockerBundle\OfflineLockerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LockReleaseCommand extends Command
{
    /**
     * @var OfflineLockerInterface
     */
    private $locker;

    public function __construct(OfflineLockerInterface $locker)
    {
        $this->locker = $locker;
        parent::__construct('lock:release');
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setDescription('Освобождение эксклюзивной блокировки на объект')
            ->setDefinition(
                new InputDefinition([
                    new InputOption('id', null, InputOption::VALUE_REQUIRED, 'Id объекта'),
                    new InputOption('owner', null, InputOption::VALUE_REQUIRED, 'Ключ (имя) владельца'),
                ])
            )
        ;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id    = (int) $input->getOption('id');
        $owner = $input->getOption('owner');

        if (!$this->locker->hasAnyLock($id)) {
            $output->writeln('Блокировка не найдена');
            return;
        }

        $this->locker->releaseLock($id, $owner);

        $output->writeln('Блокировка освобождена');
    }
}
