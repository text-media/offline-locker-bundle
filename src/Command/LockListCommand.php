<?php

namespace TextMedia\OfflineLockerBundle\Command;

use TextMedia\OfflineLockerBundle\Entity\Lock;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TextMedia\OfflineLockerBundle\OfflineLockerInterface;

class LockListCommand extends Command
{
    /**
     * @var OfflineLockerInterface
     */
    private $locker;

    public function __construct(OfflineLockerInterface $locker)
    {
        $this->locker = $locker;
        parent::__construct('lock:list');
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setDescription('Список блокировок');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $list = $this->locker->getRepository()->findAll();

        $table = new Table($output);
        $table->setHeaders(['id', 'Владелец', 'Создана']);

        foreach ($list as $item) {
            /** @var Lock $item */
            $table->addRow([$item->getId(), $item->getOwner(), $item->getCreatedAt()->format('Y.m.d H:i:s')]);
        }

        $output->writeln('Всего блокировок: ' . count($list));
        $table->render();
    }
}
