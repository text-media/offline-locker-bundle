<?php

namespace TextMedia\OfflineLockerBundle\Command;

use TextMedia\OfflineLockerBundle\OfflineLockerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LockAcquireCommand extends Command
{
    /**
     * @var OfflineLockerInterface
     */
    private $locker;

    public function __construct(OfflineLockerInterface $locker)
    {
        $this->locker = $locker;
        parent::__construct('lock:acquire');
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setDescription('Получение эксклюзивной блокировки на объект')
            ->setDefinition(
                new InputDefinition([
                    new InputOption('key', null, InputOption::VALUE_REQUIRED, 'Ключ объекта'),
                    new InputOption('owner', null, InputOption::VALUE_REQUIRED, 'Имя владельца'),
                ])
            )
        ;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $key   = $input->getOption('key');
        $owner = $input->getOption('owner');

        $this->locker->acquireLock($key, $owner);

        $output->writeln('Блокировка захвачена');
    }
}
