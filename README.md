# Offline Locker Bundle

[![Packagist](https://img.shields.io/packagist/v/text-media/offline-locker-bundle.svg)](https://packagist.org/packages/text-media/offline-locker-bundle)
[![Packagist](https://img.shields.io/packagist/l/text-media/offline-locker-bundle.svg)](https://packagist.org/packages/text-media/offline-locker-bundle)

Модуль Symfony для длительных блокировок по ключам

## Установка

Модуль устанавливается через добавление зависимости в composer

```bash
composer require text-media/offline-locker-bundle
```

В конфиг нужно добавить секцию offline_locker и дополнительный конфиг doctrine для отдельного подключения 

```YAML
offline_locker:
    # Используемый менеджер
    entity_manager: offline_locker

doctrine:
    # Отдельное подключение к БД
    dbal:
        connections:
            offline_locker:
                driver: pdo_sqlite
                path: "%kernel.cache_dir%/offline_locker.db"
                charset: UTF8
    orm:
        entity_managers:
            offline_locker:
                connection: offline_locker
                naming_strategy: doctrine.orm.naming_strategy.underscore
                mappings:
                    OfflineLockerBundle: ~
```

Выполнить миграцию автоматически или вручную

```bash
php ./bin/console doctrine:schema:update --em=offline_locker --dump-sql
```

## Использование

В контейнере зарегистрирован сервис `\TextMedia\OfflineLockerBundle\OfflineLockerInterface` (или `offline_locker`) реализующий одноименный интерфейс
 
```php
<?php
    use TextMedia\OfflineLockerBundle\ConcurrencyException;
    use TextMedia\OfflineLockerBundle\OfflineLockerInterface;

    class MyService
    {
        public function __construct(OfflineLockerInterface $locker)
        {
            $lockKey = 'order::1000';
            $lockOwner = __CLASS__;
            try {
                # Захватить блокировку
                $locker->acquireLock($lockKey, $lockOwner);
            } catch (ConcurrencyException $e) {
                # Блокировка не захвачена
            }

            try {
                $this->doSomething();
            } finally {
                # Освободить блокировку
                $locker->releaseLock($lockKey, $lockOwner);
            }
        }
    }

```
