<?php

namespace TextMedia\OfflineLockerBundle\Tests\Test;

use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bridge\Doctrine\RegistryInterface;
use TextMedia\OfflineLockerBundle\ConcurrencyException;
use TextMedia\OfflineLockerBundle\Entity\Lock;
use TextMedia\OfflineLockerBundle\OfflineLocker;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class OfflineLockerTest extends KernelTestCase
{
    /**
     * @var OfflineLocker
     */
    private $offlineLocker;

    protected function setUp()
    {
        static::bootKernel();

        $container = static::$kernel->getContainer();

        $this->offlineLocker = $container->get('offline_locker');

        /** @var RegistryInterface $doctrine */
        $doctrine = $container->get('doctrine');

        /** @var string $entityManagerName */
        $entityManagerName = $container->getParameter('offline_locker.entity_manager');

        $manager  = $doctrine->getEntityManager($entityManagerName);
        $metaData = $manager->getClassMetadata(Lock::class);

        // Удалим старую таблицу, если есть, и создадим заново из сущности Text.
        $schemaTool = new SchemaTool($manager);
        $schemaTool->dropSchema([$metaData]);
        $schemaTool->createSchema([$metaData]);
    }

    public function testAcquireLock()
    {
        $this->offlineLocker->acquireLock('order::10', 'order');
        $this->offlineLocker->acquireLock('order::11', 'order');

        try {
            $this->offlineLocker->acquireLock('order::10', 'order');
            $this->assertTrue(false, 'Должен был случится ConcurrencyException!');
        } catch (ConcurrencyException $e) {
            $this->offlineLocker->acquireLock('order::12', 'order');
        }
    }

    public function testReleaseLock()
    {
        $this->offlineLocker->acquireLock('order::10', 'order');
        $this->offlineLocker->acquireLock('order::11', 'order');
        $this->offlineLocker->releaseLock('order::11', 'order');
        $this->offlineLocker->acquireLock('order::11', 'order');

        try {
            $this->offlineLocker->acquireLock('order::10', 'order');
            $this->assertTrue(false, 'Должен был случится ConcurrencyException!');
        } catch (ConcurrencyException $e) {
            $this->offlineLocker->acquireLock('order::12', 'order');
        }

        try {
            $this->offlineLocker->acquireLock('order::11', 'order');
            $this->assertTrue(false, 'Должен был случится ConcurrencyException!');
        } catch (ConcurrencyException $e) {
        }
    }

    public function testHasLock()
    {
        $this->offlineLocker->acquireLock('order::10', 'order');
        $this->offlineLocker->acquireLock('order::11', 'offer');
        $this->offlineLocker->releaseLock('order::11', 'offer');

        $this->assertTrue($this->offlineLocker->hasLock('order::10', 'order'));
        $this->assertFalse($this->offlineLocker->hasLock('order::10', 'offer'));
        $this->assertFalse($this->offlineLocker->hasLock('order::11', 'offer'));
    }

    public function testHasAnyLock()
    {
        $this->offlineLocker->acquireLock('order::10', 'order');
        $this->offlineLocker->acquireLock('order::11', 'order');
        $this->offlineLocker->releaseLock('order::11', 'order');

        $this->assertTrue($this->offlineLocker->hasAnyLock('order::10'));
        $this->assertFalse($this->offlineLocker->hasAnyLock('order::11'));
    }
}
